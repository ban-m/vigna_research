library("tidyverse")

#### ======== Load Data =========
banded <- read_csv("./result/test_banded.tsv") %>%
    mutate(score = ifelse(score < 0,-1,score)) 
full_alignment <- read_csv("./result/test.tsv")

#### ========= Marge Data =======
scores  <- full_join(x = full_alignment %>% select(ID,score) %>% rename(full=score),
                     y = banded %>% select(ID,score) %>% rename(banded=score))


####  ===== Main proc ======

### Should be null tibble(banded is a heuristics)
scores %>% filter(full < banded)
