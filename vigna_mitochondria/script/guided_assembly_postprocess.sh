#!/bin/bash
#$ -S /bin/bash
#$ -N PostProcess
#$ -cwd
#$ -pe smp 24
#$ -e ./logfiles/guided_assembly_postprocess.log
#$ -o ./logfiles/guided_assembly_postprocess.out
#$ -V
#$ -m e

set -ue

### ==== Result =====



### ==== Post process of guided assembly =====

#### ==== Mapping back =====
OUTPUT=~/work/vigna_research/vigna_mitochondria/result/mapback
mkdir -p ${OUTPUT}
function mapback () {
    OUTPUT=~/work/vigna_research/vigna_mitochondria/result/mapback
    CONTIG=~/work/vigna_research/vigna_mitochondria/result/assembly/${2}_flye/scaffolds.fasta
    minimap2 -t 24 -x map-pb -a ${CONTIG} ${1} | \
        samtools view -hbS -q 50  |\
        samtools sort -@ 2 -m 4G -O BAM > ${OUTPUT}/${2}.mapback.bam
}
export -f mapback

### ==== Summarize =====
function copy () {
    echo $1
    filename=${1##./result/assembly/}
    filename=${filename%/scaffolds.fasta}
    echo ${filename}.fasta
    cp $1 ./result/assemblies/${filename}.fasta
}

export -f copy

mkdir -p ./result/assemblies/
cp ./result/assembly/*/*.contigs.fasta ./result/assemblies/
find ./result/assembly/ -name scaffolds.fasta | parallel copy {}
bbb sequence_statistics --input ~/work/vigna_research/vigna_mitochondria/result/assemblies/* > \
    ./result/assembly_stats.dat

# { echo "" ; find ./result/ -name "scaffolds.fasta"; } > ./result/flye_assembleies.name
# paste ./result/flye_assembleies.name ./result/flye_assembleies.temp > ./result/flye_assembleies.tsv

# find ~/data/vigna/raw_reads/ -name "*.fq" | \
#     parallel -j 1 mapback {} {/.}

# bbb sequence_statistics --input \
#     ./result/assembly/V_*_canu/*contigs.fasta \
#     > ./result/canu_assemblies.tsv
