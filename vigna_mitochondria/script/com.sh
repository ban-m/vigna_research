#!/bin/bash
cat ./result/filtered_mungo_test.fq |\
    paste - - - - |\
    grep  $1 |\
    cut -f 1,2 |\
    tr '\t' '\n' |\
    sed -e 's/@/>/g' > ./result/untrimed/${1}.fa
