#!/bin/bash
#$ -S /bin/bash
#$ -N Name
#$ -cwd
#$ -pe smp 12
#$ -e ./logfiles/coverage_plot.log
#$ -o ./logfiles/coverage_plot.out
#$ -V
# -m e
## job template

mkdir ./result/depth/

function coverage_plot() {
    samtools depth $1 > ./result/depth/${2}.tsv
    R --vanilla --slave ./script/coverage_plot.R ./result/depth/${2}.tsv ${2}
}

export -f coverage_plot

find ./result/mapback/ -name *.bam | \
    parallel coverage_plot {} {./}
