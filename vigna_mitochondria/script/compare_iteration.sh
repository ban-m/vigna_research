#!/bin/bash
for species in ~/data/vigna/raw_reads/*
do
    species=${species%.fq}
    species=${species#${HOME}/data/vigna/raw_reads/}
    echo ${species} "original-canu"
    cargo run --release --bin compare_readset -- ./result/filtered_reads/${species}.fq ./result/filtered_reads/${species}_canu.fq
    echo ""
    echo ${species} "original-flye"
    cargo run --release --bin compare_readset -- ./result/filtered_reads/${species}.fq ./result/filtered_reads/${species}_flye.fq
    echo ""
    echo ${species} "canu-flye"
    cargo run --release --bin compare_readset -- ./result/filtered_reads/${species}_canu.fq ./result/filtered_reads/${species}_flye.fq
    echo ""
done
