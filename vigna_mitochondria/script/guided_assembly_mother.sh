#!/bin/bash
#$ -S /bin/bash
#$ -N MitoAsm
#$ -cwd
#$ -pe smp 12
#$ -V
#$ -e ./logfiles/mitoasm.log
#$ -o ./logfiles/mitoasm.out
#$ -m e
# -M banmasutani@gmail.com ## To send a mail when ended
# -t 1:n ## For array job
set -ue

for read in ~/data/vigna/raw_reads/*.fq 
do
    name=${read%.fq}
    name=${name#~/data/vigna/raw_reads/}
    echo $name
    qsub -N $name \
         -e ./logfiles/${name}.log \
         -o ./logfiles/${name}.out \
         ./script/guided_assembly.job \
         $read $name
done

# qsub -N test \
#      -e ./logfiles/asm.log \
#      -o ./logfiles/asm.out \
#      ./script/guided_assembly.job \
#      ~/data/vigna/raw_reads/V_exilis.fq \
#      V_exilis

# bbb sequence_statistics --input ${FILTERED}/*.fq >> ./result/read_summary.tsv
