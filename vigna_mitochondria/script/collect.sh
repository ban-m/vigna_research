#!/bin/bash

mkdir -p ./result/flye/ ./result/canu

function collect () {
    cp ./result/assembly/${1}_canu/guided_asm_canu_${1}.contigs.fasta ./result/canu/${1}_canu.fasta
    cp ./result/assembly/${1}_canu/guided_asm_canu_${1}.contigs.gfa ./result/canu/${1}_canu.gfa
    cp ./result/assembly/${1}_flye/scaffolds.fasta ./result/flye/${1}_flye.fasta
    cp ./result/assembly/${1}_flye/assembly_graph.gfa ./result/flye/${1}_flye.gfa
}

export -f collect

find ~/data/vigna/raw_reads/ -name "*.fq" | parallel collect {/.}
