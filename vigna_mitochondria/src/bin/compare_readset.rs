extern crate bio;
use bio::io::fastq;
use std::path::Path;
use std::collections::HashSet;
fn main()->std::io::Result<()>{
    let args:Vec<_> = std::env::args().collect();
    let first_id:HashSet<_> = fastq::Reader::from_file(&Path::new(&args[1]))?
        .records()
        .filter_map(|e|e.ok())
        .map(|e|e.id().to_string())
        .collect();
    let second_id:HashSet<_> = fastq::Reader::from_file(&Path::new(&args[2]))?
        .records()
        .filter_map(|e|e.ok())
        .map(|e|e.id().to_string())
        .collect();
    let inboth = first_id.intersection(&second_id).count();
    let infirst = first_id.len();
    let insecond = second_id.len();
    println!("InBoth\tInOnlyFirst\tInOnlySecond");
    println!("{}\t{}\t{}",inboth,infirst-inboth,insecond-inboth);
    Ok(())
}
