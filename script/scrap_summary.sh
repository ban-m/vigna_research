#!/bin/bash

## This script would generate read summarys of scrap reads in ./result/scrap_summary.tsv
set -ue

function procedure () {
    INPUT=$1
    OUTPUT=$2
    echo $1 $2 1>&2 
    ## total base pair, N50, average, estimated coverage.
    cargo run --release --bin statistics -- ${INPUT} 500000000 ${OUTPUT}
}

echo -e "name\tread_num\tmax_len\tmin_len\tN50\ttotal_base\taverage\testimated_coverage" > ./result/scrap_stats.tsv
export -f procedure
find /grid/ban-m/scraps_sequences/ -name \*.fq | \
    parallel -k procedure {} {/.} >> ./result/scrap_stats.tsv 
