#!/bin/bash
#$ -S /bin/bash
#$ -N Check
#$ -cwd
#$ -pe smp 12
#$ -e ./log
#$ -o ./out
#$ -V
#$ -m e
# -M banmasutani@gmail.com ## To send a mail when ended
# -t 1:n ## For array job

function check () {
    echo "$1"
    cat $1 | grep -v "@" | wc 
}

export -f check
find /grid/ban-m/aggregated_sequences/ -name *.fq | parallel -k check > ./check_summary.log
