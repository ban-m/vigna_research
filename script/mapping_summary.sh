#!/bin/bash

function summarize () {
    spname=$1
    spname=${spname#"/grid/ban-m/vigna_species/mapped_reads/"}
    spname=${spname%".stats"}
    echo -ne $spname "\t" | sed -e 's/_/./g'
    cat $1 | grep ^SN | cut -f2- | ./target/release/mapping_stats 
}
export -f summarize
echo "\begin{table}[h]";
echo "\begin{tabular}{rlll} \toprule \\\\";
echo -e "name & mapped reads(%) & mapped base(%) & mapped cigar(%) \\\\ \\midrule"
find /grid/ban-m/vigna_species/mapped_reads/ -name *.stats | parallel -k summarize {} | \
    awk 'BEGIN{
             OFMT="%.2f";
             OFS="& ";
        }
        {
            ORS=" \\\\\n";
            print "\\textit{"$1"}",1*$2,1*$3,1*$4;
        }'
echo "\bottomrule";
echo "\end{tabular}";
echo "\caption{Caption}";
echo "\label{tab:mapping-stats}";
echo "\end{table}";


