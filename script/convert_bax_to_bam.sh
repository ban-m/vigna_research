#!/bin/bash

TEMPLATE=~/work/job_template.job
DEST_ROOT=/grid/ban-m/datasets

function convert_target () {
    JOB_NAME=$1
    SOURCE=$2
    TARGET=$3
    mkdir -p $TARGET
    cat $TEMPLATE | sed -e "s/Name/${JOB_NAME}/g" >  ./script/$JOB_NAME.job
    cargo run --release --bin collect_all_bax -- ${SOURCE} ${TARGET} >> ./script/${JOB_NAME}.job
    /grid/sgeadmin/bin/lx24-amd64/qsub -q all.q ./script/${JOB_NAME}.job
}

# ===== For each species =======

# convert_target v_marina \
#                /grid/ban-m/hdd2/backup/Vigna_angularis/pacbio/V_marina/ \
#                ${DEST_ROOT}/V_marina/raw_reads/

# convert_target  v_stipulacea \
#                 /grid/ban-m/hdd2/backup/Vigna_angularis/pacbio/V_stipulacea/ \
#                 ${DEST_ROOT}/V_stipulacea/raw_reads/


# convert_target v_exilis \
#                /grid/ban-m/hdd1/backup/Vigna_angularis/pacbio/V_exilis/ \
#                ${DEST_ROOT}/V_exilis/raw_reads/

# convert_target v_indica \
#                /grid/ban-m/hdd1/backup/Vigna_angularis/pacbio/V_indica \
#                ${DEST_ROOT}/V_indica/raw_reads/ 
               
# convert_target v_minima \
#                /grid/ban-m/hdd1/backup/Vigna_angularis/pacbio/V_minima \
#                ${DEST_ROOT}/V_minima/raw_reads/
              
# convert_target v_mungo \
#                /grid/ban-m/hdd1/backup/Vigna_angularis/pacbio/V_mungo \
#                ${DEST_ROOT}/V_mungo/raw_reads/

# convert_target v_riukiuensis \
#                /grid/ban-m/hdd1/backup/Vigna_angularis/pacbio/V_riukiuensis \
#                ${DEST_ROOT}/V_riukiuensis/raw_reads/

# convert_target v_trilobata \
#                /grid/ban-m/hdd1/backup/Vigna_angularis/pacbio/V_trilobata \
#                ${DEST_ROOT}/V_trilobata/raw_reads/

# convert_target v_vexillata \
#                /grid/ban-m/hdd1/backup/Vigna_angularis/pacbio/V_vexillata \
#                ${DEST_ROOT}/V_vexillata/raw_reads/

# Illumina
# drwxr-xr-x 5 ban-m users   47 Nov 30 23:03 V_aridicola
# drwxr-xr-x 7 ban-m users   73 Dec  1 01:55 V_nakashimae
# drwxr-xr-x 4 ban-m users   32 Dec  1 02:45 V_trilobataS3
# drwxr-xr-x 7 ban-m users   73 Dec  1 06:47 V_vexillataV1_2

# PacBio
# drwxr-xr-x  79 ban-m users 4096 Dec  1 12:29 V_exilis
# drwxr-xr-x 124 ban-m users 8192 Dec  1 17:36 V_indica
# drwxr-xr-x  59 ban-m users 4096 Dec  1 20:08 V_minima
# drwxr-xr-x   8 ban-m users 4096 Dec  1 23:53 V_mungo
# drwxr-xr-x   6 ban-m users 4096 Dec  2 06:18 V_riukiuensis
# drwxr-xr-x   8 ban-m users 4096 Dec  2 10:04 V_trilobata
# drwxr-xr-x   7 ban-m users 4096 Dec  2 14:28 V_vexillata
# drwxr-xr-x 8 ban-m users 4096 Nov 30 23:40 V_marina
# drwxr-xr-x 6 ban-m users 4096 Dec  1 07:04 V_stipulacea
