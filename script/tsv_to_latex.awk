#!/bin/awk -f 
## Convert input tsv to latex table(fixed column)
BEGIN{
    print "\\begin{table}[h]";
    print "\\begin{tabular}{rllllll} \\toprule";
    OFMT="%.2f";
    OFS="& ";
}
NR==1{
    ORS="\\\\  \\midrule \n";
    gsub(/\_/,"")
    print $1,$2"(M)",$3"(K)",$5"(K)",$6"(G)",$7"(K)","coverage";
}
NR!=1{
    ORS=" \\\\\n";
    gsub(/\_/,".",$1);
    print "\\textit{"$1"}",$2/1000000,$3/1000,$5/1000,$6/1000000000,$7/1000,$8*1;
}
END{
    ORS="\n";
    print "\\bottomrule";
    print "\\end{tabular}";
    print "\\caption{Caption}";
    print "\\label{tab:rs2-subreads}";
    print "\\end{table}";
}

    
