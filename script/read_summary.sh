#!/bin/bash
# Read Summary
set -ue

## This would produce the read summary for each vigna species sequence.

function procedure () {
    INPUT=$1
    OUTPUT=$2
    echo $1 $2 1>&2 
    ## Hex plot of read length and quality value.
    cargo run --release --bin quality_value -- ${INPUT} > ./result/${OUTPUT}_readlen_quality.tsv
    Rscript --vanilla --slave ./script/hexplot.R ./result/${OUTPUT}_readlen_quality.tsv ${OUTPUT} 
    
    ## total base pair, N50, average, estimated coverage.
    cargo run --release --bin statistics -- ${INPUT} 500000000 ${OUTPUT}
}

echo -e "name\tread_num\tmax_len\tmin_len\tN50\ttotal_base\taverage\testimated_coverage" > ./result/read_stats.tsv
export -f procedure
find /grid/ban-m/aggregated_sequences/ -name \*.fq | \
    parallel -k procedure {} {/.} >> ./result/read_stats.tsv 
