#!/bin/bash

## Wrapper script of convert_bam_to_fq.job.

QSUB="/grid/sgeadmin/bin/lx24-amd64/qsub -q all.q ./script/convert_scrap_bam_to_fq.job"

function aggregate (){
    FROM=/grid/ban-m/datasets/$1/
    TO=/grid/ban-m/scraps_sequences/$1.fq
    echo "collect from $FROM to $TO"
    ${QSUB} $FROM $TO
}
mkdir -p /grid/ban-m/scraps_sequences/
aggregate V_exilis
aggregate V_indica
aggregate V_marina
aggregate V_minima
aggregate V_mungo
aggregate V_riukiuensis
aggregate V_stipulacea
aggregate V_trilobata
aggregate V_vexillata
