use std::io::prelude::*;
fn main(){
    let input = {
        let mut buffer = String::new();
        let mut input = std::io::stdin();
        input.read_to_string(&mut buffer).unwrap();
        buffer
    };
    println!("{}",summary(input));
}

fn summary(input:String) -> String{
    let (num_reads,num_mapped_reads,
         num_base,num_mapped_base,num_mapped_cigar) = parse(&input).unwrap();
    format!("{}\t{}\t{}",
            100. * num_mapped_reads as f64 / num_reads as f64,
            100. * num_mapped_base as f64 / num_base as f64,
            100. * num_mapped_cigar as f64 / num_base as f64)
}

fn parse(input:&str) -> Result<(u64,u64,u64,u64,u64),std::num::ParseIntError>{
    let contents:Vec<_> = input
        .lines()
        .map(|e|e.split("\t").collect::<Vec<_>>())
        .collect();
    let num_reads = contents.iter().filter(|e|e[0].starts_with("sequences"))
        .nth(0).map(|e|e[1]).unwrap().parse()?;
    let num_mapped_reads = contents.iter().filter(|e|e[0].starts_with("reads mapped"))
        .nth(0).map(|e|e[1]).unwrap().parse()?;
    let num_base = contents.iter().filter(|e|e[0].starts_with("total length"))
        .nth(0).map(|e|e[1]).unwrap().parse()?;
    let num_mapped_base = contents.iter().filter(|e|e[0].starts_with("bases mapped:"))
        .nth(0).map(|e|e[1]).unwrap().parse()?;
    let num_mapped_cigar = contents.iter().filter(|e|e[0].starts_with("bases mapped (cigar)"))
        .nth(0).map(|e|e[1]).unwrap().parse()?;
    Ok((num_reads,
     num_mapped_reads,
     num_base,
     num_mapped_base,
     num_mapped_cigar))
}


//raw total sequences:	6622473
//sequences:	6622473
//is sorted:	1
//1st fragments:	6622473
//reads mapped:	4626430
//reads unmapped:	1996043
//reads MQ0:	42216	# mapped and MQ=0
//non-primary alignments:	10925016
//total length:	53128849435	# ignores clipping
//bases mapped:	42878594627	# ignores clipping
//bases mapped (cigar):	10165884737	# more accurate
//average length:	8022
//maximum length:	88695
//average quality:	255.0


#[test]
fn test(){
    let input = r#"raw total sequences:	6622473
sequences:	6622473
is sorted:	1
1st fragments:	6622473
reads mapped:	4626430
reads unmapped:	1996043
reads MQ0:	42216	# mapped and MQ=0
non-primary alignments:	10925016
total length:	53128849435	# ignores clipping
bases mapped:	42878594627	# ignores clipping
bases mapped (cigar):	10165884737	# more accurate
average length:	8022
maximum length:	88695
average quality:	255.0"#;
    let (a,b,c,d,e) = parse(input).unwrap();
    assert_eq!(a,6622473);
    assert_eq!(b,4626430);
    assert_eq!(c,53128849435);
    assert_eq!(d,42878594627);
    assert_eq!(e,10165884737);
}
    
