use std::fs;
use std::fs::DirEntry;
use std::path::Path;
fn main() -> std::io::Result<()> {
    let args: Vec<_> = std::env::args().collect();
    println!("BAX2BAM=/bio/package/pacbio/smrtlink/smrtcmds/bin/bax2bam");
    println!("{}", collect_all_bax(args)?);
    Ok(())
}

fn collect_all_bax(args: Vec<String>) -> std::io::Result<String> {
    let destination_dir = Path::new(&args[2]);
    let source_dir = Path::new(&args[1]);
    search_current_directly(&source_dir, &source_dir, &destination_dir)
}

fn search_current_directly(initial: &Path, current: &Path, dest: &Path) -> std::io::Result<String> {
    // Make a script to convert bax to bam.
    // If there are some bax file, it pruned current search,
    // create a script to replicate the directly structure from 'current' to destination, to
    // envoke "bax2bam" in that directly, and to return the initial(=destination) directly.
    // Else, it keep searching.
    assert!(current.is_dir());
    assert!(dest.is_dir());
    let dir_entries: Vec<_> = fs::read_dir(&current)?.filter_map(|e| e.ok()).collect();
    let res = if has_bax_h5(&dir_entries) {
        // Start
        get_command_to_collect_bax(initial, current, dest, &dir_entries)
    } else {
        dir_entries
            .into_iter()
            .filter(|e| is_ok_path(e) )
            .filter_map(|e| search_current_directly(initial, e.path().as_path(), dest).ok())
            .fold(String::new(), |acc, x| acc + &x)
    };
    Ok(res)
}

fn is_ok_path(entry:&DirEntry)->bool{
    let filetype = match entry.file_type() {
        Ok(res) => res.is_dir(),
        Err(_why) => false,
    };
    let prefix = match entry.path().to_str(){
        Some(res) if !res.starts_with('_') => true,
        _ => false
    };
    filetype && prefix
}

fn get_command_to_collect_bax(
    initial: &Path,
    current: &Path,
    dest: &Path,
    entries: &[DirEntry],
) -> String {
    let (mut command, mut raw_read_dir) = (String::new(), std::path::PathBuf::new());
    raw_read_dir.push(dest);
    raw_read_dir.push(current.strip_prefix(initial).unwrap());
    command.push_str(&format!(
        "mkdir -p {} \ncd {} \n${{BAX2BAM}} {} \n",
        raw_read_dir.display(),
        raw_read_dir.display(),
        get_all_bax_h5(&entries)
    ));
    command
}

fn get_all_bax_h5(entires: &[DirEntry]) -> String {
    entires
        .iter()
        .filter_map(|entry| match entry.path().to_str() {
            Some(res) if res.ends_with(".bax.h5") => Some(res.to_string()),
            _ => None,
        })
        .fold(String::new(), |acc, x| acc + &x + " ")
}

fn has_bax_h5(entries: &[DirEntry]) -> bool {
    entries
        .iter()
        .filter(|e| match e.file_type() {
            Ok(res) => res.is_file(),
            Err(_why) => false,
        })
        .any(|e| match e.path().to_str() {
            Some(res) => res.ends_with(".bax.h5"),
            None => false,
        })
}
