extern crate bio;
use std::path::Path;
use bio::io::fastq;
fn main()->std::io::Result<()>{
    let args:Vec<_> = std::env::args().collect();
    let mut lens:Vec<_> = fastq::Reader::from_file(&Path::new(&args[1]))?
    .records()
        .filter_map(|e|e.ok())
        .map(|e|e.seq().len())
        .collect();
    lens.sort();
    let estimated_genome_size:usize = args[2].parse().unwrap();
    let tot:usize = lens.iter().sum();
    let max = lens.iter().max().unwrap();
    let min = lens.iter().min().unwrap();
    let n_fifty = {
        let (mut sofar,mut idx) = (0,0);
        while sofar < tot/2{
            sofar += lens[idx];
            idx += 1;
        }
        lens[idx-1]
    };
    let mean = tot as f64 / lens.iter().count() as f64;
    let coverage = tot as f64 / estimated_genome_size as f64;
    println!("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}",
             args[3],
             lens.len(),
             max,
             min,
             n_fifty,
             tot,
             mean,
             coverage);
    Ok(())
}
