extern crate bio;
use bio::io::fastq;
use std::path::Path;
const OFFSET:f64 = 33.0;
fn main()->std::io::Result<()>{
    let args:Vec<_> = std::env::args().collect();
    println!("read_length\taverage_quality");
    fastq::Reader::from_file(&Path::new(&args[1]))?
    .records()
        .filter_map(|e|e.ok())
        .map(|record|summarize(&record))
        .for_each(|(len,quality)|println!("{}\t{}",len,quality));
    Ok(())
}


fn summarize(record:&fastq::Record)->(usize,f64){
    let len = record.seq().len();
    let qual:f64 = record.qual().iter().map(|e|*e as usize).sum::<usize>() as f64;
    (len,qual as f64 / len as f64 - OFFSET)
}
