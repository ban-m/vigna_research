## Vigna research

Author:Bansho Masutani
Mail: ban-m@g.ecc.u-tokyo.ac.jp


## Aim

To collect all bax files from RSII dataset and make a tiny script to convert all bax'es to bams.

## NOTE
This script is to replicate the directly structure, thus, it would be a little bit combersome.
Make sure that the script would be executed, not only generated.

## Synopsis
```
cat /work/ban-m/job_template.job > ./collect.job
cargo run --release --bin colleact_all_bax -- [Source directly] [Destination directly] >> ./collect.job
qsuball ./collect.job
```

Note that the source directly and destination directly should be absolute pathes.
And the directly starting  with "_" would be ignored (because there are some direclty _old which contains a lot of junk files).
