const width = 1200;
const height = 1000;
const margin = 10;
const bar_height = 50;
const name_padding = 100;
const width_margin = 50;
const top_margin = 200;
const bar_width = width - name_padding - width_margin;
const is_zero = num => num != 0;
const transpose = m => m[0].map((x,i) => m.map(x => x[i]));
const partition = function(data, header){
    let core_gene = new Array();
    let other_gene = new Array();
    const column = data.getColumnCount();
    //console.log("Total orthologous group:", data.getRowCount());
    data.getRows().forEach((row,index)=>{
        let row_data = Array.from({length:column},(x,i) => i)
            .map(idx => row.getNum(idx));
        if (row_data.every(is_zero)){
            core_gene.push(row_data);
        }else{
            other_gene.push(row_data);
        }
    });
    //console.log("core group:",core_gene.length);
    //console.log("others:",other_gene.length);
    //console.log(core_gene[0].length);
    core_gene.sort((a,b)=> b[10] - a[10]);
    other_gene.sort((a,b)=> b[10] - a[10]);
    //console.log(other_gene[0]);
    //console.log(core_gene[0]);
    const num_of_core_genes = core_gene.length;
    const converted_data = core_gene.concat(other_gene);
    // const max = converted_data.map(row => row.reduce((a,b)=> Math.max(a,b)))
    //       .reduce((a,b)=>Math.max(a,b));
    const max = 3;
    const data_with_name = transpose(converted_data)
          .map((d,i) => {
              return {
                  data:d,
                  name:header[i]
              }});
    data_with_name.sort((a,b)=>{
        const nameA = a.name.toUpperCase();
        const nameB = b.name.toUpperCase();
        if (nameA > nameB) {
            return 1;
        } else if (nameA === nameB){
            return 0;
        }else{
            return -1;
        }
    });
    return [data_with_name, num_of_core_genes, max];
}

const plot_dataset = function(path, id, header){
    // Drawing a bar plot for a species into a specified box.
    let s = function(p){
        const from = p.color(218, 165, 32);
        const to = p.color(72, 61, 139);
        const drawHeader = function(cp, num_of_groups, coregene_num){
            p.push();
            p.textStyle(p.NORMAL);
            p.textSize(20);
            p.textAlign(p.LEFT,p.CENTER);
            p.text("color code of each bin:",20,20);
            p.text("Blank: average in the bin < 0.9",40,50);
            p.text("0.9:",40,80);
            p.text("4.0~",160,80);
            for (let x = 80; x < 160 ; x ++){
                const color = 0.9 + (3.0 - 0.9)/80 * (x-80);
                //console.log(color);
                p.stroke(cp(color));
                p.line(x,60,x+1,100);
            }
            p.stroke(0,0,0);
            p.text("Total orthogropus: " + num_of_groups, 20,120);
            p.text("Core gene: " + coregene_num, 20, 140);
            p.pop();
        }
        const draw_species_in = function(data_with_name, top, left, cp){
            const name = data_with_name.name;
            console.log(name)
            p.stroke(0,0,0);
            p.textAlign(p.RIGHT,p.CENTER);
            p.text(name, left-width_margin, top + bar_height/2);
            let data;
            if (name === "XTotal" || name === "Xtotal"){
                data = data_with_name.data.map(d => d / (header.length-1));
            }else{
                data = data_with_name.data;
            };
            //console.log(data.length);
            const plot_data = Array.from({length:bar_width},(d,i)=>i)
                  .map(idx =>{
                      const start = Math.floor(idx * data.length / bar_width);
                      const end = Math.floor((idx + 1) * data.length / bar_width);
                      const mean = data.slice(start,end).reduce((a,b) => a+b);
                      return mean / (end - start);
                  });
            //console.log(data);
            //console.log(plot_data);
            plot_data.forEach((count,index) => {
                p.stroke(cp(count));
                p.line(left + index,top,left + index + 1,top+bar_height);
            })
        }
        let raw_data;
        p.preload = function(){
            raw_data = p.loadTable(path,'tsv','header');
        }
        p.setup = function(){
            raw_data.removeColumn(0);
            p.strokeWeight(1);
            p.textStyle(p.ITALIC);
            p.textSize(15);
            p.colorMode(p.RGB);
            p.background(0);
            p.createCanvas(width, height);
            p.noLoop();
        }
        const drawBoundary = function(coregene_num, num_of_groups, datasize){
            const boundary = name_padding +
                  width_margin +
                  coregene_num * bar_width/num_of_groups;
            const top = top_margin - 20;
            const btm = top_margin + datasize * (bar_height + margin) - margin;
            console.log(boundary,top,btm);
            p.push();
            p.strokeWeight(10);
            p.stroke(0,100);
            p.strokeCap(p.SQUARE);
            p.line(boundary,top,boundary,btm);
            p.pop();
        }
        p.draw = function(){
            const [data, coregene_num, max_num] = partition(raw_data, header);
            const cp = function(count){
                if (count < 0.9) {
                    return p.color(255,255,255);
                }else if (count > max_num){
                    return to;
                }else {
                    const x = (count - 0.9) / (3.0-0.9);
                    return p.lerpColor(from, to, x/max_num);
                }
            };
            const num_of_groups = data[0].data.length;
            drawHeader(cp,num_of_groups, coregene_num);
            drawBoundary(coregene_num, num_of_groups, data.length);
            data.forEach((species, index)=>{
                const top = top_margin + index * (bar_height + margin);
                const left = name_padding + width_margin;
                console.log("ha");
                draw_species_in(species, top, left, cp);
            });
        }
    };
    return new p5(s,id);
}

