//!Automatically selecting paralogous variant sites from a given BAM file.
//!
//!This module serves for picking up informative variants and filtering out uninformative variants from numerous SNP in the raw read pileups.
//!
//!Specifically, this module have only one public available struct, namely `PSV`, and one public function named [`get_psv_from_bam`].

use rust_htslib::bam;
use rust_htslib::bam::Read;
use rust_htslib::bam::Reader;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::io;
use std::path::PathBuf;

///The struct for a single PSV. PSV set for a putative duplicated region would be expressed by a vector of PSV.
///
///Actually, this struct should take a reference for the bam file tied with. This is because each PSV is represented by, essentially, a tuple of (target ID, position of PSV, its major base, its minor base). To make target ID meaningful, the "code" for the ID is needed, which is in the header of the BAM file.
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct PSV {
    /// Tagrget ID. To retrieve the actual reference name, see the header file of the BAM file from which the instance came.
    pub tid: u32,
    /// Position of PSV. 0-based Index.
    pub position: u32,
    /// Major allele of the PSV. One of b"ATGC".(upper case only)
    pub major: u8,
    /// Minor allele of the PSV. One of b"ATGC".
    pub minor: u8,
    /// Minor allel count
    pub mac: u32,
    /// Major allel count
    pub maj: u32,
    /// depth
    pub depth: u32,
}

impl PSV {
    /// Get the position of PSV.
    pub fn position(&self) -> u32 {
        self.position
    }
    pub fn new(
        tid: u32,
        position: u32,
        major: u8,
        maj: u32,
        minor: u8,
        mac: u32,
        depth: u32,
    ) -> Self {
        PSV {
            tid,
            position,
            major,
            minor,
            mac,
            maj,
            depth,
        }
    }
}

fn not_found() -> std::io::Error {
    io::Error::new(io::ErrorKind::NotFound, "bam file does not exist.")
}

type Reads = HashMap<Vec<u8>, Vec<u8>>;

/// Determining PSV from a given BAM file and returns the set of PSV inside the region.
/// Note that this function only accept the BAM file *extracted* boforehand.
pub fn get_psv_from_bam(
    path: PathBuf,
    mean: u32,
    sd: f64,
    reads: &Reads,
) -> std::io::Result<Vec<PSV>> {
    if !path.exists() {
        Err(not_found())
    } else {
        Ok(get_psv_from_records(
            Reader::from_path(path).unwrap(),
            mean as f64,
            sd,
            reads,
        ))
    }
}

fn get_psv_from_records(reader: Reader, mean: f64, sd: f64, reads: &Reads) -> Vec<PSV> {
    let pileups: Vec<(u32, u32, (Vec<u8>, u32))> = get_pileups(reader, reads);
    into_psv_set(pileups, mean, sd)
}

fn get_pileups(mut reader: Reader, reads: &Reads) -> Vec<(u32, u32, (Vec<u8>, u32))> {
    reader
        .pileup()
        .filter_map(|pileup| pileup.ok())
        .map(|pileup| (pileup.tid(), pileup.pos(), extract_bases(&pileup, reads)))
        .collect()
}

fn extract_bases(pileup: &bam::pileup::Pileup, _reads: &Reads) -> (Vec<u8>, u32) {
    let seq: Vec<_> = pileup
        .alignments()
        .filter_map(|e| extract_base_from_record(&e))
        .collect();
    (seq, pileup.depth())
}

fn extract_base_from_record(alignment:&bam::pileup::Alignment)->Option<u8>{
    let is_primary = alignment.record().flags() & 0x900 == 0;
    let len = alignment.record().seq().len();
    match alignment.qpos(){
        Some(pos) if is_primary && len != 0 => Some(alignment.record().seq()[pos]),
        _ => None,
    }
}

// Below this, all functions are pure rust, not dependes on htslib.

fn into_psv_set(dataset: Vec<(u32, u32, (Vec<u8>, u32))>, mean: f64, sd: f64) -> Vec<PSV> {
    dataset
        .into_iter()
        .filter_map(|(tid, pos, (pileup, depth))| {
            eprint!("{}\t{}\t{}", tid, pos, depth);
            collect_psv_from_pileup(&pileup, depth, mean, sd).map(|(maj, maj_count, mac, mac_count)| {
                PSV::new(tid, pos, maj, maj_count, mac, mac_count,depth)
            })
        })
        .collect()
}

fn collect_psv_from_pileup(pileup: &[u8], depth: u32, mean: f64, sd: f64) -> Option<(u8, u32, u8, u32)> {
    let (major, maj, minor, mac) = get_mac_from_seqs(pileup);
    let macf = mac as f64;
    if depth as f64 > mean + 3. * sd
        && macf > 5.
        && macf < mean
        && (mean / 2. < macf || macf > mean - (sd * 3.))
    {
        Some((major, maj, minor, mac))
    } else {
        None
    }
}

fn get_mac_from_seqs(seqs: &[u8]) -> (u8, u32, u8, u32){
    let mut freqs: [(u32, u8); 4] = seqs.iter().fold(
        [(0, b'A'), (0, b'C'), (0, b'G'), (0, b'T')],
        |mut acc, base| {
            match base {
                b'A' | b'a' => acc[0].0 += 1,
                b'C' | b'c' => acc[1].0 += 1,
                b'G' | b'g' => acc[2].0 += 1,
                b'T' | b't' => acc[3].0 += 1,
                _ => {}
            };
            acc
        },
    );
    freqs.sort_by_key(|e| e.0);
    // To extract Minor allel frequency, use [2].
    // (major allel, major count, minor allel, minor count)
    (freqs[3].1, freqs[3].0, freqs[2].1, freqs[2].0)
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn works() {
        assert!(true);
    }
    #[test]
    fn from_maf() {
        let _depth = 100;
        let seqs = vec![b'A'; 100];
        let (major, _maj_count, _minor, freq) = get_mac_from_seqs(&seqs);
        assert!(freq == 0);
        assert_eq!(major, b'A');
    }
    #[test]
    fn from_maf2() {
        let _depth = 150;
        let mut seqs = vec![b'A'; 100];
        seqs.extend(vec![b'C'; 50]);
        let (major, _major_count, minor, freq) = get_mac_from_seqs(&seqs);
        assert!(freq == 50);
        assert_eq!(major, b'A');
        assert_eq!(minor, b'C');
    }
    #[test]
    fn collect_psv() {
        let depth = 100;
        let pileup: Vec<_> = (0..depth)
            .map(|e| match e {
                x if x < 50 => b'A',
                x if x < 80 => b'T',
                x if x < 95 => b'C',
                _x => b'G',
            })
            .collect();
        let mean = 25.;
        let sd = 4.;
        // should return none, because the maf is too high.
        assert_eq!(None, collect_psv_from_pileup(&pileup, depth, mean, sd));
    }
    #[test]
    fn collect_psv_2() {
        let depth = 100;
        let pileup: Vec<_> = (0..depth)
            .map(|e| match e {
                x if x < 90 => b'A',
                x if x < 95 => b'T',
                x if x < 100 => b'C',
                _x => b'G',
            })
            .collect();
        let mean = 25.;
        let sd = 4.;
        // should return none, bacause the maf is too small.
        assert_eq!(None, collect_psv_from_pileup(&pileup, depth, mean, sd));
    }
    #[test]
    fn collect_psv_3() {
        let depth = 100;
        let pileup: Vec<_> = (0..depth)
            .map(|e| match e {
                x if x < 80 => b'A',
                x if x < 95 => b'T',
                x if x < 100 => b'C',
                _x => b'G',
            })
            .collect();
        let mean = 25.;
        let sd = 4.;
        // should return Some, bacause the maf satisfy all criteria.
        assert_eq!(
            Some((b'A', 80, b'T', 15)),
            collect_psv_from_pileup(&pileup, depth, mean, sd)
        );
    }
    #[test]
    fn collect_psv_4() {
        let depth = 100;
        let pileup: Vec<_> = (0..depth)
            .map(|e| match e {
                x if x < 80 => b'A',
                x if x < 91 => b'T',
                x if x < 100 => b'C',
                _x => b'G',
            })
            .collect();
        let mean = 25.;
        let sd = 7.;
        // should return Some, because the maf satisfies the upper criteria and
        // one of the two additional criteria.
        assert_eq!(
            Some((b'A', 80, b'T', 11)),
            collect_psv_from_pileup(&pileup, depth, mean, sd)
        );
    }
    #[test]
    fn collect_psv_5() {
        let depth = 100;
        let pileup: Vec<_> = (0..depth)
            .map(|e| match e {
                x if x < 80 => b'A',
                x if x < 98 => b'T',
                x if x < 100 => b'C',
                _x => b'G',
            })
            .collect();
        let mean = 25.;
        let sd = 2.;
        // should return Some, because the maf satisfies the upper criteria and
        // one of the two additional criteria.
        assert_eq!(
            Some((b'A', 80, b'T', 18)),
            collect_psv_from_pileup(&pileup, depth, mean, sd)
        );
    }
    #[test]
    fn collect_psv_6() {
        let depth = 100;
        let pileup: Vec<_> = (0..depth)
            .map(|e| match e {
                x if x < 70 => b'A',
                x if x < 90 => b'T',
                x if x < 100 => b'C',
                _x => b'G',
            })
            .collect();
        let mean = 25.;
        let sd = 2.;
        // should return Some, because the maf satisfies the upper criteria and
        // one of the two additional criteria.
        assert_eq!(
            Some((b'A', 70, b'T', 20)),
            collect_psv_from_pileup(&pileup, depth, mean, sd)
        );
    }
    #[test]
    fn collect_psv_set() {
        let mean = 25.;
        let sd = 4.;
        let dataset = (0..20)
            .map(|i| {
                let depth = 100;
                let pileup: Vec<_> = (0..depth)
                    .map(|e| match e {
                        x if x < 90 => b'A',
                        x if x < 95 => b'T',
                        x if x < 100 => b'C',
                        _x => b'G',
                    })
                    .collect();
                (i as u32, i as u32, (pileup, depth))
            })
            .collect();
        assert_eq!(into_psv_set(dataset, mean, sd), vec![]);
    }
    #[test]
    fn collect_psv_set2() {
        let length = 100;
        let mean = 25.;
        let sd = 4.;
        let dataset = (0..length)
            .map(|i| {
                let depth = 100;
                let pileup: Vec<_> = (0..depth)
                    .map(|e| match e {
                        x if x < 80 => b'A',
                        x if x < 95 => b'T',
                        x if x < 100 => b'C',
                        _x => b'G',
                    })
                    .collect();
                (i as u32, i as u32, (pileup, depth))
            })
            .collect();
        let res = into_psv_set(dataset, mean, sd);
        // should return some set with its length remains the same.
        assert_eq!(res.len(), length);
    }
}
