use rayon::prelude::*;
use rust_htslib::bam;
use rust_htslib::prelude::*;
use std::path::Path;
const MARGIN: u32 = 300;
const WIDTH: u32 = 5_000;
// Get contig names and thier id, bundled with lengths.
fn get_contig_names_with_id_and_length<T>(reader: &T) -> Vec<(String, u32, u32)>
where
    T: bam::Read,
{
    let header = reader.header();
    header
        .target_names()
        .iter()
        .filter_map(|name| {
            Some((
                String::from_utf8_lossy(name).to_string(),
                header.tid(name)?,
                header.target_len(header.tid(name)?)?,
            ))
        })
        .collect()
}

fn get_depth<T>(reader: &mut T) -> Vec<(u32, u32)>
where
    T: bam::Read,
{
    let mut depth: Vec<_> = reader
        .pileup()
        .filter_map(|e| e.ok())
        .map(|e| (e.pos(), get_depth_of_a_position(&e)))
        .collect();
    depth.sort();
    depth
}

fn get_depth_of_a_position(pileup: &bam::pileup::Pileup) -> u32 {
    pileup
        .alignments()
        .filter(|e| e.record().flags() & 0x900 == 0)
        .count() as u32
}

pub fn get_thresholds(path: &str) -> Result<(f64, f64, u32, u32), String> {
    let mut bamfile = bam::IndexedReader::from_path(path).map_err(|e| e.to_string())?;
    let contigs = get_contig_names_with_id_and_length(&bamfile);
    let data: Vec<_> = contigs
        .into_iter()
        .map(|(_, tid, length)| {
            bamfile.fetch(tid, 0, length).unwrap();
            (tid, get_depth(&mut bamfile))
        })
        .collect();
    Ok(calc_statistics(&data))
}

/// Collect all the duplicated region as the format of
/// Vec<(target it, start position, end position)>.
/// It is designed to be used in understream analysis.
/// The criteria used in this function is as follows:
/// a region is a segmental duplication if and only if
///   - at all position i with depth d in that region, d is above mean + 3 * s.d after triming upper and lower 2% as outlier
///   - the width of that region exceeds 5Kbp.
pub fn collect_collapsed_region(path: &Path) -> Result<Vec<(u32, u32, u32)>, String> {
    let mut bamfile = bam::IndexedReader::from_path(path).map_err(|e| e.to_string())?;
    let contigs = get_contig_names_with_id_and_length(&bamfile);
    let data = contigs
        .into_iter()
        .map(|(_, tid, length)| {
            bamfile.fetch(tid, 0, length).unwrap();
            (tid, get_depth(&mut bamfile))
        })
        .collect();
    Ok(collect_collapsed_region_from_depth(data))
}

/// Return type is (tid, start, end), not (tid, start index, end index) of wig format
/// This caution is crucial when there is a position with 0 depths.
fn collect_collapsed_region_from_depth(data: Vec<(u32, Vec<(u32, u32)>)>) -> Vec<(u32, u32, u32)> {
    let (mean, sd, upper, lower) = calc_statistics(&data);
    data.into_iter()
        .flat_map(|(tid, depths)| {
            get_collapsed_region(&depths, mean, sd, upper, lower)
                .into_iter()
                .map(|(start, end)| (tid, depths[start].0, depths[end].0))
                .filter(|&(_tid, start, end)| end - start > WIDTH)
                .collect::<Vec<_>>()
        })
        .collect()
}

/// Dump the duplicated region as Vec<(Contig name, Vec<(position,depth)>.
/// It is designed to just plot and overviewing, thus it also produce the
/// joining region, i.e., 300 bp consective region before and after the detected region.
/// The criteria used is that, a position i with depth d is segmental duplicated if
/// and only if d is above mean + 3 * s.d,
/// after triming upper and lower 2% as outlier.
pub fn dump_collapsed_region(path: &Path) -> Result<Vec<(String, Vec<(u32, u32)>)>, String> {
    let mut bamfile = bam::IndexedReader::from_path(path).map_err(|e| e.to_string())?;
    bamfile.set_threads(12).unwrap();
    let contigs = get_contig_names_with_id_and_length(&bamfile);
    let data: Vec<(String, Vec<(u32, u32)>)> = contigs
        .into_iter()
        .map(|(name, tid, length)| {
            bamfile.fetch(tid, 0, length).unwrap();
            (name, get_depth(&mut bamfile))
        })
        .collect();
    Ok(dump_collapsed_region_from_depth(data))
}

fn dump_collapsed_region_from_depth(
    data: Vec<(String, Vec<(u32, u32)>)>,
) -> Vec<(String, Vec<(u32, u32)>)> {
    // Retaining all entries (i,d) s.t.
    // there is a entry (j,e) satisfying e > depths + 3*sd && lower <= e && e <= upper
    let (mean, sd, upper, lower) = calc_statistics(&data);
    let mut res = vec![];
    data.into_par_iter()
        .map(|(name, depths)| {
            let starts_and_ends = get_collapsed_region(&depths, mean, sd, upper, lower);
            let regions = marge_collapsed_region(starts_and_ends, &depths);
            let result: Vec<_> = regions
                .into_iter()
                .filter(|&(start, end)| depths[end].0 - depths[start].0 < WIDTH)
                .flat_map(|(start, end)| depths[start..end].to_vec())
                .collect();
            (name, result)
        })
        .collect_into_vec(&mut res);
    res
}

fn marge_collapsed_region(
    regions: Vec<(usize, usize)>,
    depths: &[(u32, u32)],
) -> Vec<(usize, usize)> {
    let mut res = vec![];
    if regions.is_empty() {
        return res;
    }
    let (mut previous_start, mut previous_end) = regions[0];
    for &(start, end) in regions[1..].iter() {
        if depths[start].0 < depths[previous_end].0 + MARGIN {
            previous_end = end;
        } else {
            if depths[previous_start].0 < MARGIN {
                res.push((0, previous_end + MARGIN as usize));
            } else {
                res.push((
                    previous_start - MARGIN as usize,
                    previous_end + MARGIN as usize,
                ));
            }
            previous_start = start;
            previous_end = end;
        }
    }
    res.push((previous_start, previous_end));
    res
}

pub fn calc_statistics<T>(data: &[(T, Vec<(u32, u32)>)]) -> (f64, f64, u32, u32) {
    let mut depths: Vec<u32> = data
        .iter()
        .flat_map(|(_, data)| data.iter().map(|(_, depth)| *depth))
        .collect();
    depths.sort();
    calc_mean_var_upper_lower(depths)
}
fn calc_mean_var_upper_lower(depths: Vec<u32>) -> (f64, f64, u32, u32) {
    let length = depths.len();
    let upper = depths[(length as f64 * 0.95).floor() as usize];
    let lower = depths[(length as f64 * 0.05).floor() as usize];
    // eprintln!("Triming upper and lower 2% of the data...");
    // eprintln!("Length:{}\tUpper:{}\tLower:{}", depths.len(), upp
    // er, lower);
    let (sum, sumsq, length) = depths
        .into_iter()
        .filter(|&d| lower <= d && d <= upper)
        .fold((0u64, 0u64, 0u64), |acc, x| {
            (acc.0 + x as u64, acc.1 + (x * x) as u64, acc.2 + 1)
        });
    let mean = sum as f64 / length as f64;
    let var = (sumsq as f64 / length as f64) - mean * mean;
    (mean, var.sqrt(), upper, lower)
}

fn get_collapsed_region(
    depths: &[(u32, u32)],
    mean: f64,
    sd: f64,
    upper: u32,
    lower: u32,
) -> Vec<(usize, usize)> {
    // Return (index of start position,index of end position)
    let threshold = (mean + 3. * sd).floor() as u32;
    let is_duplicate = |depth| threshold <= depth && lower <= depth && depth <= upper;
    let (mut start_index, mut is_in_duplicate, mut regions) = (0, false, vec![]);
    for (index, (_position, depth)) in depths.iter().enumerate() {
        if is_in_duplicate {
            if !is_duplicate(*depth) {
                is_in_duplicate = false;
                regions.push((start_index, index));
            }
        } else {
            if is_duplicate(*depth) {
                is_in_duplicate = true;
                start_index = index;
            }
        }
    }
    regions
}
#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn statistics_check() {
        let data: Vec<_> = (0..100).collect();
        let (mean, sd, _, _) = calc_mean_var_upper_lower(data);
        assert!((mean - 50.).abs() < 0.001);
        assert!((sd - 26.26785).abs() < 0.001);
    }
}
