use super::super::rust_htslib::bam;
use super::super::rust_htslib::bam::Read;
use super::super::segmental_duplicate::psv::PSV;
use super::super::rust_htslib::bam::record::Cigar;

use serde::{Serialize,Deserialize};

fn cumulative_log(size:usize)->Vec<f64>{
    let mut res = Vec::with_capacity(size);
    let mut current = 0.;
    for ln in  (0..size).map(|e| (e as f64).ln()){
        res.push(current);
        current += ln;
    }
    res
}


#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum Entry{
    Major,
    Minor,
    Other,
}

/// Construct PSV graph from PSV set and original bam.
/// To this end, the given bam file reads sequencialy,
/// picking PSVs one by one.
/// The intermidiate matrix M s.t. M_{i,j} indicates
/// whether j-th read contains i-th PSV.
pub fn construct_psv_graph(bam_path:&str, psvs:&Vec<PSV>)-> std::io::Result<(Vec<Option<Entry>>, usize,usize)>{
    let bam_path = std::path::Path::new(bam_path);
    if bam_path.exists(){
        use std::io;
        Err(io::Error::new(io::ErrorKind::NotFound, "file does not exist."))
    }else{
        let read_number = bam::Reader::from_path(&bam_path)
            .unwrap().records().filter_map(|e|e.ok()).count();
        let psv_size = psvs.len();
        let mut read_psv_matrix:Vec<Option<Entry>> = vec![None;read_number*psv_size];
        for (read_index, record) in bam::Reader::from_path(&bam_path)
            .unwrap().records()
            .filter_map(|e|e.ok()).enumerate(){
                if record.seq().len() == 0{
                    continue
                };
                let (start,end) = get_start_and_end_position(&record,psvs);
                let reference_state = get_reference_state(&record);
                for index in start..end{
                    let psv = &psvs[index];
                    let pos = psv.position() as usize;
                    read_psv_matrix[read_number * index + read_index ] = match reference_state[pos]{
                    x if x == psv.major => Some(Entry::Major),
                    x if x == psv.minor => Some(Entry::Minor),
                    x if x == b'D' => Some(Entry::Other),
                    _ => None,
                };
            }
        }
        Ok((read_psv_matrix,psv_size, read_number))
    }
}

fn get_start_and_end_position(record:&bam::Record, psvs:&Vec<PSV>)->(usize,usize){
    let start = record.pos() as u32;
    let end = record.cigar().end_pos().unwrap() as u32;
    let start = match psvs.binary_search_by_key(&start,|e|e.position()){
        Ok(res) => res,
        Err(res) => res,
    };
    let end = match psvs.binary_search_by_key(&end, |e|e.position()){
        Ok(res) => res,
        Err(res) => res,
    };
    (start as usize, end as usize)
}

fn get_reference_state(record:&bam::Record)->Vec<u8>{
    let mut res = vec![];
    let seq = record.seq().as_bytes();
    let mut current_query_index:usize = 0;
    for op in record.cigar().iter(){
        match op{
            Cigar::SoftClip(l) => current_query_index += *l as usize,
            Cigar::Match(l) | Cigar::Diff(l) | Cigar::Equal(l) => {
                let match_region = &seq[current_query_index..(current_query_index + *l as usize)];
                res.extend_from_slice(match_region);
                current_query_index += *l as usize;
            },
            Cigar::RefSkip(l) | Cigar::Del(l) => {
                res.extend(vec![b'D';*l as usize]);
            },
            Cigar::Ins(l) => current_query_index += *l as usize,
            Cigar::Pad(_) | Cigar::HardClip(_) => {},
        }
    }
    res
}

/// Row: number of PSV
/// Column: number of reads
pub fn read_psv_graph_to_psvgraph(graph:Vec<Option<Entry>>,row:usize, column:usize, mean:u32)->
    (Vec<i8>,usize){
    let mut res = vec![0;row*row];
    let ln_cumulative = cumulative_log(column);
    for i in 0..row {
        for j in i..row{
            let (maj_maj, maj_min, min_maj, min_min) = graph[i*row..(i+i)*row].iter()
                .zip(graph[j*row..(j+1)*row].iter())
                .filter(|(ith,jth)|ith.is_some() && jth.is_some())
                .map(|(ith,jth)|(ith.unwrap(),jth.unwrap()))
                .map(|(ith,jth)| match (ith, jth) {
                    (Entry::Major, Entry::Major) => (1,0,0,0),
                    (Entry::Major, Entry::Minor) => (0,1,0,0),
                    (Entry::Minor, Entry::Major) => (0,0,1,0),
                    (Entry::Minor, Entry::Minor) => (0,0,0,1),
                    _ => (0,0,0,0)
                })
                .fold((0,0,0,0),|(a,b,c,d),(x,y,z,w)|(a+x,b+y,c+z,d+w));
            let total = maj_maj + maj_min + min_maj + min_min;
            let ith_maj = maj_maj + maj_min;
            let jth_maj = maj_maj + min_maj;
            let ith_min = min_min + min_maj;
            let jth_min = min_min + maj_min;
            let invariant = -2. * (total as f64).ln() +
                (ith_maj as f64)*(ith_maj as f64).ln() +
                (jth_maj as f64)*(jth_maj as f64).ln() +
                (ith_min as f64)*(ith_min as f64).ln() +
                (jth_min as f64)*(jth_min as f64).ln();
            let sampled = ln_cumulative[total] - (ln_cumulative[maj_maj] +
                                                  ln_cumulative[maj_min] +
                                                  ln_cumulative[min_maj] +
                                                  ln_cumulative[min_min]);
            let p_value:f64 = (0..ith_maj.min(jth_maj))
                .map(|x| ln_cumulative[total] - (ln_cumulative[x] +
                                                 ln_cumulative[ith_maj - x] +
                                                 ln_cumulative[jth_maj - x] +
                                                 ln_cumulative[total + x - ith_maj - jth_maj]))
                .filter(|&x| x < sampled)
                .fold(0.,|acc,x| acc + x) + invariant;
            if p_value < (0.05_f64).ln(){
                res[i*row + j] = 1;
                res[j*row + i] = 1;
                continue;
            }
            if min_min < mean as usize / 10 {
                res[i*row + j] = -1;
                res[j*row + i] = -1;
                continue;
            }
        }
    }
    (res, row)
}
