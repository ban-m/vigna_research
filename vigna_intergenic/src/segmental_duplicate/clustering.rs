use std::collections::HashSet;
use rand::thread_rng;
use rand::Rng;
const THR:f64 = 0.5;

pub fn clustering(psvgraph:&Vec<i8>,size:usize)->Vec<HashSet<usize>>{
    let (mut res,mut best_score)  = (vec![], 100);
    for _ in 0..15{
        let result = clustering_try(psvgraph,size);
        let score = calculate_score_of_clusters(psvgraph,size,&result);
        if score < best_score{
            res = result;
            best_score = score;
        }
    }
    res
}

fn calculate_score_of_clusters(psvgraph:&Vec<i8>,size:usize,clusters:&Vec<HashSet<usize>>)->i32{
    clusters.iter().map(|cluster| calculate_score(psvgraph,size,cluster))
        .sum()
}

fn calculate_score(psvgraph:&Vec<i8>,size:usize, cluster:&HashSet<usize>)->i32{
    (0..size).map(|u|(0..size).map(|v|{
        let is_u = cluster.contains(&u);
        let is_v = cluster.contains(&v);
        if is_u && is_v{
            -1 * psvgraph[u*size + v].min(0) as i32
        }else if is_u || is_v{
            psvgraph[u*size + v].max(0) as i32
        }else{
            0
        }
    }).fold(0,|acc,x| acc + x)).fold(0,|acc,x| acc + x)
}

fn clustering_try(psvgraph:&Vec<i8>,size:usize)->Vec<HashSet<usize>>{
    let draft_clusters = get_draft_cluster(psvgraph,size);
    let marged_clusters = merge_clusters(draft_clusters);
    (0..size).map(|e|(e,assign(e,&marged_clusters)))
        .fold(vec![HashSet::with_capacity(marged_clusters.len())],|mut acc,(index,assign)|{
            acc[assign].insert(index);
            acc})
}


fn get_draft_cluster(psvgraph:&Vec<i8>,size:usize)->Vec<HashSet<usize>>{
    let mut done:HashSet<_> = HashSet::new();
    let mut nodes = (0..size).collect::<Vec<_>>();
    let (mut rng, mut res) = (thread_rng(),vec![]);
    rng.shuffle(&mut nodes);
    for seed in nodes{
        if done.contains(&seed){
            continue;
        }else{
            done.insert(seed);
            let cluster = construct_cluster(seed, psvgraph,size);
            for node in &cluster{
                done.insert(*node);
            }
            res.push(cluster);
        }
    }
    res
}

fn merge_clusters(mut draft_clusters:Vec<HashSet<usize>>)->Vec<HashSet<usize>>{
    draft_clusters.sort_by_key(|e|e.len());
    let mut res = vec![];
    'outer: while let Some(item) = draft_clusters.pop(){
        for pair in draft_clusters.iter_mut(){
            let intersection =  item.intersection(pair).count();
            let union = item.union(pair).count();
            if intersection as f64 / union  as f64 > THR {
                pair.extend(item);
                continue 'outer;
            }
        }
        res.push(item);
    }
    res
}

fn assign(index:usize, clusters:&Vec<HashSet<usize>>)-> usize{
    clusters.iter().enumerate().filter(|(_, cluster)|cluster.contains(&index))
        .map(|(cluster_idx, cluster)|(cluster_idx, cluster.len()))
        .max_by_key(|e|e.1).unwrap().0
}

fn construct_cluster(seed:usize, psvgraph:&Vec<i8>,size:usize)->HashSet<usize>{
    let mut cluster = HashSet::new();
    cluster.insert(seed);
    let mut neighbors :HashSet<usize> = psvgraph[seed*size..(seed+1)*size].iter()
        .enumerate()
        .filter(|&(_,&e)|e >= 0)
        .map(|(index,_)|index)
        .collect();
    let mut is_updated = true;
    while is_updated{
        let neighbor = neighbors.iter().filter(|&e|!cluster.contains(e))
            .filter_map(|e|if will_score_be_better(&cluster, psvgraph, size,*e){
                Some(e)
            }else{
                None
            }).nth(0);
        is_updated = neighbor.is_some();
        if !is_updated{
            break;
        }else{
            let neighbor = neighbor.unwrap();
            cluster.insert(*neighbor);
            for neighbor in psvgraph[neighbor*size..(neighbor+1)*size].iter().enumerate()
                .filter(|(_,&weight)|weight > 0)
                .map(|(index,_)|index){
                    neighbors.insert(neighbor);
                }
        }
    }
    cluster
}

fn will_score_be_better(cluster:&HashSet<usize>, psvgraph:&Vec<i8>, size:usize,add:usize)->bool{
    let increasing = psvgraph[add*size..(add+1)*size].iter().enumerate()
        .filter(|(_index,&weight)| weight != 0)
        .map(|(index, &weight)|
             if cluster.contains(&index){
                 // If it has positive weight, it gives a reward.
                 // else, it incurs damage.
                 -1*weight 
             }else{
                 weight.max(0)
             })
        .fold(0, |acc,x| acc + x);
    increasing < 0 
}

