//! Module to investigate intergenic and genic regions.
// To collect (intergenic, genebody, intergenic) triples.
pub mod gene_collection;
// To convert multiple sequence alignments into a sequence divergence tables.
pub mod convert_msa;
