function get_op(name,place){
    const url = "../../result/orthogroups/orthofinder/3000-3999/OG0004181/" +
          name +
          ".msa"
    console.log(url);
    return {
        el: document.getElementById(place),
        vis: {
            conserv: false,
            overviewbox: false
    },
        // smaller menu for JSBin
        menu: "small",
        bootstrapMenu: true,
        importURL:url,
    };
}


var up = get_op("upstream","UPSTREAM");
var up_m = new msa.msa(up);
up_m.render();
var body = get_op("genebody","GENEBODY");
var body_m = new msa.msa(body);
body_m.render();

var down = get_op("downstream","DOWNSTREAM");
var down_m = new msa.msa(down);
down_m.render();
