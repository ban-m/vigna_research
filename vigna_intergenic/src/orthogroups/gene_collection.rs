use bio::io::fasta;
use bio::io::gff;
use bio_types::strand;
use std::collections::HashMap;
use std::io::Result;
use std::io::{BufRead, BufReader};
use std::path::Path;
// All genes too near to the boundary would be exluded.
const CLIP: usize = 2_000;
#[derive(Debug)]
struct Configure {
    species_name: String,
    genome: Vec<fasta::Record>,
    genes: HashMap<String, gff::Record>,
}

impl Configure {
    fn open_gff(file: &String) -> Result<HashMap<String, gff::Record>> {
        eprintln!("Opening GFF file from {}...", file);
        let result: HashMap<_, _> = gff::Reader::from_file(&Path::new(file),
                                                           gff::GffType::GFF3)?
            .records()
            .filter_map(|e| e.ok())
            .filter(|e| e.feature_type() == "mRNA")
            .filter_map(|e| {
                if let Some(id) = e.attributes().get("ID") {
                    Some((id.to_string(), e))
                } else {
                    eprintln!("No ID:{:?}", e);
                    None
                }
            })
            .collect();
        eprintln!("Collected {} entries",result.len());
        Ok(result)
    }
    fn open_genome(file:&str)->Result<Vec<fasta::Record>> {
        eprintln!("Opening genome from {:?}",file);
        let result:Vec<_> = fasta::Reader::from_file(&Path::new(file))?
            .records()
            .filter_map(|e| e.ok())
            .collect();
        eprintln!("Collected {} entries",result.len());
        Ok(result)
    }
    fn is(&self, name: &str) -> bool {
        self.species_name == name
    }
    fn get_gff(&self, genename: &str) -> Option<&gff::Record> {
        self.genes.get(genename)
    }
    fn get_contig(&self, contigname: &str) -> Option<&fasta::Record> {
        self.genome.iter().find(|&contig| contig.id() == contigname)
    }
    fn new<R: std::io::Read>(reader: R) -> std::io::Result<Self> {
        let config: HashMap<String, String> = serde_json::de::from_reader(reader)?;
        let species_name = config["name"].to_string();
        let genome = Self::open_genome(&config["genome"])?;
        eprintln!("Genome opened");
        let genes = Self::open_gff(&config["genes"])?;
        eprintln!("Gff opended");
        Ok(Configure {
            species_name,
            genome,
            genes,
        })
    }
}

#[derive(Debug)]
struct Orthogroups {
    header: Vec<String>,
    names: Vec<String>,
    contents: Vec<Vec<String>>,
    row: usize,
    column: usize,
}

impl Orthogroups {
    fn from_path<P: AsRef<Path>>(path: P) -> Result<Self> {
        let (mut header, mut contents, mut names) = (vec![], vec![], vec![]);
        BufReader::new(std::fs::File::open(path)?)
            .lines()
            .filter_map(|e| e.ok())
            .enumerate()
            .for_each(|(linenum, buf)| {
                if linenum == 0 {
                    header = Self::parse_header(&buf);
                } else {
                    for (index, entry) in buf.split('\t').enumerate() {
                        if index == 0 {
                            names.push(entry.to_string());
                        } else {
                            contents.push(Self::parse_group(entry));
                        }
                    }
                }
            });
        let (row, column) = (names.len(), header.len());
        assert_eq!(contents.len() / row, column);
        Ok(Orthogroups {
            header,
            names,
            contents,
            row,
            column,
        })
    }
    fn parse_header(buf: &str) -> Vec<String> {
        // Skip 1 for Orthofinder
        // The header file is encoded like
        // "exilis_protein", thus, one should trim the trailing
        // "_protein"
        buf.replace("_proteins", "")
            .split('\t')
            .skip(1)
            .map(|e| e.to_string())
            .collect()
    }
    fn parse_group(buf: &str) -> Vec<String> {
        // parsing is needed to convert CDS naming into gene naming.
        // this is only effective for angularis,
        // but harmless to other annotations.
        buf.split(',')
            .filter_map(|e| e.split('_').nth(0))
            .map(|e| e.to_string())
            .collect()
    }
    fn iter(&self) -> OrthogroupsIter {
        OrthogroupsIter {
            orthogroups: &self,
            pointer: 0,
        }
    }
    fn header(&self) -> &[String] {
        &self.header
    }
    /// Get i-th row.
    fn get(&self, index: usize) -> &[Vec<String>] {
        let start = index * self.column;
        let end = (index + 1) * self.column;
        &self.contents[start..end]
    }
    fn rowlen(&self) -> usize {
        self.row
    }
    /// Get the i-th orthogroup name.
    fn name(&self, index: usize) -> &str {
        &self.names[index]
    }
}

#[derive(Debug)]
struct OrthogroupsIter<'a> {
    orthogroups: &'a Orthogroups,
    pointer: usize,
}

impl<'a> std::iter::Iterator for OrthogroupsIter<'a> {
    type Item = (&'a [String], &'a str, &'a [Vec<String>]);
    fn next(&mut self) -> Option<Self::Item> {
        if self.pointer >= self.orthogroups.rowlen() {
            None
        } else {
            let header = self.orthogroups.header();
            let contents = self.orthogroups.get(self.pointer);
            let name = self.orthogroups.name(self.pointer);
            self.pointer += 1;
            Some((header, name, contents))
        }
    }
}

pub fn convert_orthogroups_into_genetriples(
    orthogroups: &str,
    species_file: &[String],
) -> Result<Vec<(String, Vec<[fasta::Record; 3]>)>> {
    eprintln!("Start converting");
    let orthogroups = Orthogroups::from_path(&Path::new(orthogroups))?;
    eprintln!("Parsed orthogroups. Total:{} groups.", orthogroups.rowlen());
    let species_files = open_species_file(species_file)?;
    eprintln!("Parsed species files.{} Species", species_files.len());
    let gene_triples: Vec<_> = orthogroups
        .iter()
        .filter_map(|(header, name, cont)| into_triples(header, name, cont, &species_files))
        .collect();
    Ok(gene_triples)
}

fn open_species_file(species_file: &[String]) -> Result<Vec<Configure>> {
    let mut res = vec![];
    for f in species_file {
        eprintln!("opening {}...",f);
        res.push(open_specification(f)?);
        eprintln!("Done");
    }
    Ok(res)
}

fn open_specification(config_file: &str) -> Result<Configure> {
    let file = std::fs::File::open(&Path::new(config_file))?;
    Configure::new(file)
}

fn into_triples(
    header: &[String],
    name: &str,
    cont: &[Vec<String>],
    species: &[Configure],
) -> Option<(String, Vec<[fasta::Record; 3]>)> {
    let mut triples = vec![];
    for (genenames, spname) in cont.iter().zip(header.iter()) {
        let configure: &Configure = species.iter().find(|&e| e.is(spname)).unwrap();
        let sp_triples: Vec<_> = genenames
            .iter()
            .filter_map(|genename| extract_sequence(genename, configure, name))
            .collect();
        if sp_triples.is_empty() {
            return None;
        } else {
            triples.extend(sp_triples.into_iter());
        }
    }
    Some((name.to_string(), triples))
}

fn extract_sequence(genename: &str, configure: &Configure, id: &str) -> Option<[fasta::Record; 3]> {
    let gff = configure.get_gff(genename)?;
    let (scf, start, end, is_revcomp) = parse_gff(gff)?;
    let contig = configure.get_contig(scf)?;
    let id = format!("{}_{}", id, genename);
    let (up, body, down) = extract_from_contig(contig, start, end, is_revcomp)?;
    let rec = |sig,seq| fasta::Record::with_attrs(&id, Some(sig), seq);
    Some([rec("UP", &up), rec("BODY", &body), rec("DOWN", &down)])
}

fn parse_gff(gff: &gff::Record) -> Option<(&str, usize, usize, bool)> {
    let start = *gff.start() as usize;
    let end = *gff.end() as usize;
    let is_revcomp = match gff.strand() {
        Some(strand::Strand::Reverse) => true,
        Some(strand::Strand::Forward) => false,
        _ => {
            eprintln!("{:?}", gff);
            false
        }
    };
    Some((gff.seqname(), start.min(end), start.max(end), is_revcomp))
}

fn extract_from_contig(
    contig: &fasta::Record,
    start: usize,
    end: usize,
    is_revcomp: bool,
) -> Option<(Vec<u8>, Vec<u8>, Vec<u8>)> {
    let seq = contig.seq();
    let len = seq.len();
    if len <= end + CLIP || start < CLIP {
        None
    } else if !is_revcomp {
        Some((
            seq[start - CLIP..start].to_vec(),
            seq[start..end].to_vec(),
            seq[end - CLIP..end].to_vec(),
        ))
    } else {
        use bio::alphabets::dna::revcomp;
        Some((
            revcomp(&seq[start - CLIP..start]),
            revcomp(&seq[start..end]),
            revcomp(&seq[end - CLIP..end]),
        ))
    }
}
#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn can_open_configure() {
        let file = "./result/specification/exilis.json";
        let file = std::fs::File::open(&Path::new(file)).unwrap();
        assert!(Configure::new(file).is_ok());
    }
    #[test]
    fn can_open_configure2() {
        let file = "./result/specification/exilis.json";
        let file = std::fs::File::open(&Path::new(file)).unwrap();
        let file = Configure::new(file).unwrap();
        assert!(file.is("exilis"));
        assert!(file.get_gff("no").is_none());
        assert!(file.get_contig("no").is_none());
        assert!(file.get_gff("Vigex.0001s000200.01").is_some());
        assert!(file.get_contig("scf0001").is_some());
    }
    #[test]
    fn can_open_file1() {
        let file = "./data/test_data/pseudo_orthologous.dat";
        assert!(Orthogroups::from_path(&Path::new(file)).is_ok());
        assert!(Orthogroups::from_path(&Path::new("")).is_err());
    }
    #[test]
    fn can_open_file2() {
        let file = "./data/test_data/pseudo_orthologous.dat";
        let data = Orthogroups::from_path(&Path::new(file)).unwrap();
        eprintln!("{:?}", data);
        assert!(data.header().len() != 0);
        assert!(data.get(0).len() != 0);
        assert!(data.rowlen() != 0);
        assert!(data.name(0) != "");
    }
    #[test]
    fn can_open_file3() {
        let file = "./data/test_data/pseudo_orthologous.dat";
        let data = Orthogroups::from_path(&Path::new(file)).unwrap();
        eprintln!("{:?}", data);
        assert_eq!(data.iter().count(), 4);
        assert_eq!(data.header()[0], "angularis");
        assert_eq!(data.header()[1], "exilis");
        assert_eq!(data.rowlen(), 4);
        assert_eq!(data.name(1), "OG0000096");
        assert_eq!(data.get(0).len(), 10);
        assert_eq!(data.get(0)[0].len(), 2);
        let iterator = data.iter().nth(0).unwrap();
        assert_eq!(iterator.2[0].len(), 2);
    }
}
