extern crate rayon;
extern crate rust_htslib;
extern crate vigna_intergenic;
use vigna_intergenic::segmental_duplicate;
fn main() {
    let args: Vec<_> = std::env::args().collect();
    let (mean, sd, upper, lower) =
        segmental_duplicate::detection::get_thresholds(&args[1]).unwrap();
    println!("mean, sd, upper, lower");
    println!("{},{},{},{}", mean, sd, upper, lower);
}
