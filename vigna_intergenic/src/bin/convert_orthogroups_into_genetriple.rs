extern crate bio;
extern crate serde;
extern crate serde_json;
extern crate vigna_intergenic;
use bio::io::fasta;
use std::io::{BufRead, BufReader};
use std::path::Path;
use vigna_intergenic::orthogroups;
fn main() -> std::io::Result<()> {
    let args: Vec<_> = std::env::args().collect();
    let orthogroups_file = &args[1];
    let species_files: Vec<_> = BufReader::new(std::io::stdin())
        .lines()
        .filter_map(|e| e.ok())
        .collect();
    let orthogroups = orthogroups::gene_collection::convert_orthogroups_into_genetriples(
        orthogroups_file,
        &species_files,
    )?;
    for (index, (group_name, genes)) in orthogroups.into_iter().enumerate() {
        let dir_idx = index - index % 1000;
        let dirname = format!(
            "./result/orthogroups/orthofinder/{}-{}/{}",
            dir_idx,
            dir_idx + 999,
            group_name
        );
        std::fs::create_dir_all(&Path::new(&dirname))?;
        let create_writer =
            |name| fasta::Writer::to_file(&Path::new(&format!("{}/{}", &dirname, name)));
        let mut upstream = create_writer("upstream.fa")?;
        let mut genebody = create_writer("genebody.fa")?;
        let mut downstream = create_writer("downstream.fa")?;
        for [up, gene, down] in genes {
            upstream.write_record(&up)?;
            genebody.write_record(&gene)?;
            downstream.write_record(&down)?;
        }
    }
    Ok(())
}
