use std::fs::File;
use std::io::{BufWriter, Write};
use std::path::Path;
const HEADER:&str =
    "#$ -S /bin/bash
#$ -N Last-doplot
#$ -cwd
#$ -pe smp 12
#$ -e ./logfiles/last-doplot-{sp1}-{sp2}.log
#$ -o ./logfiles/last-doplot-{sp1}-{sp2}.log
#$ -V
SP1={sp1}
SP1_genome=/lustre/share/vigna/Vigna_genomes_and_annotations/${SP1}/${SP1}.fa
SP2={sp2}
SP2_genome=/lustre/share/vigna/Vigna_genomes_and_annotations/${SP2}/${SP2}.fa
cd ./data/last_database/
OUTPUT=~/work/vigna_research/vigna_intergenic/result/last_dotplot/${SP1}-${SP2}.maf
if [ ! -f ${OUTPUT} ]
then
    parallel-fasta \"lastal ${SP1}db | last-split -m 0.0008 \" < ${SP2_genome} > ${OUTPUT}
    last-dotplot -x 3000 -y 3000 ${OUTPUT} ~/work/vigna_research/vigna_intergenic/png/${SP1}-${SP2}.png
fi
";
fn main() {
    let args: Vec<_> = std::env::args().collect();
    let (_prog_name, names) = args.split_at(1);
    if let Err(code) = main_process(names) {
        std::process::exit(code);
    };
}
fn main_process(names: &[String]) -> Result<(), i32> {
    for i in 0..names.len() {
        let sp1 = format!(
            "/lustre/share/vigna/Vigna_genomes_and_annotations/{s}/{s}.fa",
            s = names[i]
        );
        if !Path::new(&sp1).exists() {
            eprintln!("Error:{} does not exists.", sp1);
            return Err(1);
        }
        for j in 0..names.len() {
            let sp2 = format!(
                "/lustre/share/vigna/Vigna_genomes_and_annotations/{s}/{s}.fa",
                s = names[j]
            );
            if !Path::new(&sp2).exists() {
                eprintln!("Error:{} does not exists.", sp2);
                return Err(1);
            }
            let output = format!("./script/last_dotplot/{}-{}.sh", names[i], names[j]);
            let mut wtr = match File::create(Path::new(&output)) {
                Ok(res) => BufWriter::new(res),
                Err(why) => {
                    eprintln!("{:?}", why);
                    return Err(1);
                }
            };
            if let Err(why) = writeln!(
                &mut wtr,
                "{}",
                HEADER
                    .replace("{sp1}", &names[i])
                    .replace("{sp2}", &names[j])
            ) {
                eprintln!("{:?}", why);
                return Err(1);
            };
            if let Err(why) = wtr.flush() {
                eprintln!("{:?}", why);
                return Err(1);
            };
        }
    }
    Ok(())
}
