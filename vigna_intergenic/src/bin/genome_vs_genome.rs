use std::path::Path;
fn main() {
    let args: Vec<_> = std::env::args().skip(1).collect();
    let (root, remainings) = args.split_at(1);
    let (species, output) = remainings.split_at(remainings.len() - 1);
    let (root, output) = (&root[0], &output[0]);
    eprintln!("ROOT:{}, OUTPUT:{}", root, output);
    for i in 0..species.len() {
        let sp1 = root.to_string() + &species[i] + "/" + &species[i] + ".fa";
        if !Path::new(&sp1).exists(){
            eprintln!("ERROR: {} does not exists.", sp1);
        }    
        for j in i..species.len() {
            let sp2 = root.to_string() + &species[j] + "/" + &species[j] + ".fa";
            if !Path::new(&sp2).exists(){
            eprintln!("ERROR: {} does not exists.", sp2);
            }    
            let output = output.to_string() + &species[i] + "_"+ &species[j] + ".paf";
            println!("minimap2 -t 24 -x asm20 {} {} > {}", sp1, sp2, output);
        }
    }
}
