extern crate vigna_intergenic;
extern crate serde;
extern crate serde_json;
extern crate bio;
use bio::io::fastq;
use serde_json::ser;
use vigna_intergenic::segmental_duplicate::psv;
use std::path::Path;
use std::io::{BufReader,BufRead};
use std::fs;
use std::collections::HashMap;
fn main() -> std::io::Result<()>{
    let args:Vec<_> = std::env::args().collect();
    let thr_path = Path::new(&args[1]);
    let path = args[1].clone() + "/threshold.dat";
    let e:Vec<f64> = BufReader::new(fs::File::open(&Path::new(&thr_path))?)
        .lines()
        .filter_map(|e|e.ok())
        .nth(1).unwrap()
        .split(',')
        .filter_map(|e|e.parse().ok())
        .collect();
    let mean:u32 = e[0].floor() as u32;
    let sd:f64 = e[1];
    let reads:HashMap<_,_> = fastq::Reader::from_file(&Path::new(&args[2]))?
        .records()
        .filter_map(|e|e.ok())
        .map(|e|(e.id().bytes().collect::<Vec<_>>(),
                 e.seq().to_vec()))
        .collect();
    let result:Vec<_> = Path::new(&path).read_dir()?.filter_map(|e|e.ok())
        .filter(|e|e.path().exists())
        .filter_map(|e|{
            let bam = e.file_name().to_str()?.to_string();
            Some((bam, psv::get_psv_from_bam(e.path(),mean,sd, &reads).ok()?))
        })
        .collect();
    println!("{}",ser::to_string(&result).unwrap());
    Ok(())
}
