extern crate serde;
extern crate serde_json;
use std::collections::HashMap;
fn main()->std::io::Result<()>{
    let args:Vec<_> = std::env::args().collect();
    let mut config = HashMap::new();
    let name = args[1].clone();
    let genome = args[2].clone();
    let gff = args[3].clone();
    config.insert("name".to_string(),name);
    config.insert("genome".to_string(),genome);
    config.insert("genes".to_string(),gff);
    let json = serde_json::ser::to_string(&config)?;
    println!("{}",json);
    Ok(())
}
