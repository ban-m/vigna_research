extern crate rust_htslib;
extern crate rayon;
extern crate vigna_intergenic;
use vigna_intergenic::segmental_duplicate;
use rust_htslib::bam;
use rust_htslib::prelude::*;
use std::path::Path;
fn main() -> std::io::Result<()> {
    let args: Vec<_> = std::env::args().collect();
    let bam_path = Path::new(&args[1]);
    let result = segmental_duplicate::detection::collect_collapsed_region(&bam_path).unwrap();
    let mut bam_file = bam::IndexedReader::from_path(&bam_path).unwrap();
    for name in bam_file.header().target_names(){
        let tid = bam_file.header().tid(name).unwrap();
        println!("{}\t{}",String::from_utf8_lossy(name),tid);
    }
    let mut record = bam::Record::new();
    if result.is_empty(){
        eprintln!("No duplicate detected:{}",&args[1]);
    }
    for (tid, start,end) in result {
        bam_file.fetch(tid, start, end).unwrap();
        let outname = format!("{}{}-{}-{}.bam",args[2],tid,start,end);
        let header = bam::Header::from_template(bam_file.header());
        let mut wtr = bam::Writer::from_path(&Path::new(&outname), &header)
            .unwrap();
        while let Ok(_res) = bam_file.read(&mut record) {
            if record.flags() & 0x900 == 0{
                wtr.write(&record).unwrap();
            }
        }
    }
    Ok(())
}

