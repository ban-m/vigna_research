extern crate bio;
extern crate rust_htslib;
extern crate rayon;
extern crate vigna_intergenic;
use vigna_intergenic::segmental_duplicate::detection;
use std::fs::File;
use std::io::BufWriter;
use std::io::Write;
use std::path::Path;
fn main() -> std::io::Result<()> {
    let args: Vec<_> = std::env::args().collect();
    let bam_path = Path::new(&args[1]);
    let result = detection::dump_collapsed_region(bam_path).unwrap();
    let mut out = BufWriter::new(File::create(&Path::new(&args[2]))?);
    for (contig_name, region) in result {
        for (index, depth) in region {
            writeln!(&mut out, "{}\t{}\t{}", contig_name, index, depth)?;
        }
    }
    out.flush()?;
    Ok(())
}
