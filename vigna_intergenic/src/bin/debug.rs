extern crate rust_htslib;
extern crate vigna_intergenic;
extern crate bio;
use bio::io;
use std::path::Path;
use std::io::{BufRead,BufReader};
use std::collections::HashMap;
#[derive(Debug)]
struct Debug{
    id:String,
    genome:Vec<io::fasta::Record>,
    hash:HashMap<String,String>
}

fn main(){
    let _args:Vec<_> = std::env::args().collect();
    let args:Vec<_> = BufReader::new(std::io::stdin())
        .lines()
        .filter_map(|e|e.ok())
        .collect();
    eprintln!("{:?}",&args);
    let mut res = vec![];
    for file in &args{
        eprintln!("Opening {} ...",file);
        let contigs:Vec<_> = io::fasta::Reader::from_file(&Path::new(file))
            .unwrap()
            .records()
            .filter_map(|e|e.ok())
            .collect();
        res.push(Debug{
            id:file.to_string(),
            genome:contigs,
            hash:HashMap::new(),
        });
        eprintln!("Done");
    }
    for contigs in res{
        eprintln!("{},{},{}",contigs.id,contigs.genome.len(),contigs.hash.len());
    }
}
