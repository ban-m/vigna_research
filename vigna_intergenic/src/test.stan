data{
  int D;
  int M;
}

parameters {
  real<lower=0,upper=1> p;
}
model{
  D ~ bionomial(M,p);
}
