### Intergenic diversity analysis

## Todo

Determin signle copy locus by observing coverage each pileup.
  - we can 

- Fuzzy multiple alignment
  - DO NOT USE ALIGNMENT PER SE
  - turning around the multiple alignmnet(it's hard to handle!)

- Statistical analysis
  - Creating model
  - Write script
  - Confirm the result

- Comparing the result to "usual" analysis
  - Using the minialign's result itself to summarize the diversity

## Done
- Mapping all fastq files into the reference genome
  - Writing script
  - Think how to handle or caveat "reference bias."
    - This may be, in fact, how I can manage with reference bias.
      In other words, not by constructing reference but by aligning raw reads,
      can we work way around the bias? By what factor?
    - Maybe no. What is important is not alignmnet, but how to treat each "fuzzy" alignment.

- Determine the single-copy locus in the reference genome(vangularis).
  - It is of course problematic. Because we can not be sure whether
    the focul gene is truly single-copy, or just misassembled and collapsed gene.
  - Yes. We cannot be sure when we just are standing on the reference.
  - However, we can surely claim "this is single-copy", by see the coverage on each gene on EACH sample.
  - So, in this place, maybe we need not filter out any gene locus.
