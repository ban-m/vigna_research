## Diversity among vigna species


## Synopsis

### last-dotplot
```bash
cargo run --release --bin last-dotplot -- [output_dir] [in.name] <in.name> ... > [script].sh
bash [script].sh
```
would execute `last` alignments for every pairs of input name in [output_dir] directly.
(Require: `png` directly, `last`, `./script/last-dotplot`)
