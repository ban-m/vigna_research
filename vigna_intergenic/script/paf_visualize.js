const width = 1000;
const height = 1000;
const margin = 50;


const map_to_array = lengths => {
    // Convert Map:<String,Int> -> Array<[String,Int]>,
    // which is NOT garanteed to be sorted.
    return Array.from(lengths);
};

const calc_cumulative_length = lengths => {
    // Convert Map:<Stirng, Int> -> []
    // Not Needed
    const sorted = map_to_array(lengths);
    let current_len = 0;
    // console.log(sorted);
    // let cumulative_len = [];
    // let ctgs = [];
    let result = new Map();
    for (const [chr, length] of sorted){
        result.set(chr,current_len);
        // ctgs.push(chr);
        // cumulative_len.push(current_len);
        // console.log(current_len);
        current_len += length;
    }
    return [result,current_len];
};

const parsePafFile = file => {
    let sp1_lengths = new Map();
    let sp2_lengths = new Map();
    let data = d3.tsvParseRows(file,(d,i) => {
        sp1_lengths.set(d[0],parseInt(d[1]));
        sp2_lengths.set(d[5],parseInt(d[6]));
        return {
            sp1_ctg_name:d[0],
            sp1_start:parseInt(d[2]),
            sp1_end:parseInt(d[3]),
            is_forward:d[4] === "+",
            sp2_ctg_name:d[5],
            sp2_start:parseInt(d[7]),
            sp2_end:parseInt(d[8]),
            num_match:parseInt(d[9]),
            alignment_length:parseInt(d[10]),
        };
    });
    const [sp1_lengths_cum, sp1_tot] = calc_cumulative_length(sp1_lengths);
    const [sp2_lengths_cum, sp2_tot] = calc_cumulative_length(sp2_lengths);
    console.log(sp1_tot);
    console.log(sp2_tot);
    const convert_to_global_position = (data) => {
        data.sp1_start = sp1_lengths_cum.get(data.sp1_ctg_name) + data.sp1_start;
        data.sp2_start = sp2_lengths_cum.get(data.sp2_ctg_name) + data.sp2_start;
        return data;
    };
    return {
        data:data.map(convert_to_global_position),
        sp1_total:sp1_tot,
        sp2_total:sp1_tot,
        sp1_lengths:sp1_lengths_cum,
        sp2_lengths:sp2_lengths_cum,
    };
};

const to_cordinate = (align,x_scale,y_scale) => {
    if (align.is_forward){
        return {
            x1:x_scale(align.sp1_start),
            x2:x_scale(align.sp1_end),
            y1:y_scale(align.sp2_start),
            y2:y_scale(align.sp2_end),
            color:"For",
        };
    }else{
        return {
            x1:x_scale(align.sp1_start),
            x2:x_scale(align.sp1_end),
            y1:y_scale(align.sp2_end),
            y2:y_scale(align.sp2_start),
            color:"Rev",
        };
    };
};

const svg = d3.select("#plot")
      .append("svg")
      .attr("width",width)
      .attr("height",height);
      

const plotData = (input_file) => 
      Promise.all([input_file].map(file => d3.text(file)))
      .then(values => {
          // Calculate corrdinates
          const paf = parsePafFile(values[0]);
          const x_scale = d3.scaleLinear()
                .domain([0,paf.sp1_total])
                .range([margin,width-margin]);
          const y_scale = d3.scaleLinear()
                .domain([0,paf.sp2_total])
                .range([height-margin,margin]);
          const x_axis = d3.axisTop(x_scale);
          const y_axis = d3.axisRight(y_scale);
          const axis = svg.append("g")
                .attr("class","axes");
          const plots = svg.append("g")
                .attr("class","plots");
          
          // Draw
          plots.selectAll(".alignment")
              .data(paf.data.map(align => to_cordinate(align,x_scale,y_scale)))
              .enter()
              .append("line")
              .attr("x1",d => d.x1)
              .attr("x2",d => d.x2)
              .attr("y1",d => d.y1)
              .attr("y2",d => d.y2)
              .attr("class",d => d.color);
          axis.append("g")
              .attr("transform","translate(0,"+(height-margin)+")")
              .call(x_axis);
          axis.append("g")
              .attr("transform","translate(" + margin + ",0)")
              .call(y_axis);
      })
      .then(ok => console.log("OK"),
            why => console.log(why));





// const calcScale = (fragments)=>{
//     const max_parent_length = fragments.filter(frag => frag.is_parent).map(frag => frag.end - frag.start)
//           .reduce((a,b)=> Math.max(a,b),1);
//     const min_parent_length = fragments.filter(frag => frag.is_parent).map(frag => frag.end - frag.start)
//           .reduce((a,b)=>Math.min(a,b),0);
//     const parent_size_scale = d3.scaleLinear().domain([min_parent_length,max_parent_length])
//           .range([min_p_size,max_p_size]);
//     return parent_size_scale
// }
    
// const calcChrCoordinate = (chrs)=>{
//     const total_length = chrs.map(chr => chr.length + radius_margin).reduce((a,b)=>a+b);
//     const radius_scale = d3.scaleLinear().domain([0,total_length]).range([0,2*Math.PI]);
//     const each_chr_interval = chrs.map(chr => chr.length).reduce((acc,chr)=>{
//         acc.res.push([acc.tot,acc.tot+chr]);
//         return {tot:acc.tot + chr + radius_margin,
//                 res:acc.res};
//     },{tot:0,res:[]});
//     return each_chr_interval.res.map(x=>[radius_scale(x[0]),radius_scale(x[1])])
//         .map((x,idx) => d3.scaleLinear().domain([0,chrs[idx].length]).range([x[0],x[1]]));
// };

// const getPath = link => {
//     let path = d3.path();
//     path.moveTo(link.source.x,link.source.y);
//     path.quadraticCurveTo(0,0,link.target.x,link.target.y);
//     //path.lineTo(link.target.x,link.target.y);
//     return path.toString();
// }


// const mouseOver = (frag,raw_links,chr_scales,parent_size_scale) => {
//     const links = getLinks(frag,raw_links,chr_scales);
//     const children = getChildren(frag,raw_links,chr_scales);
    
//     d3.select(d3.event.target)
//         .classed("active_node",true);
//     const link_handler = dyn_links
//           .append("g")
//           .attr("id","id" + frag.id)
//           .selectAll(".active_link")
//           .data(links)
//           .enter()
//           .append("path")
//           .attr("class","active_link")
//           .attr("d",getPath)
//           .attr("stroke",link => link.is_parent ? color("parent_parent_link"):color("parent_child_link"));
//     const node_handler = dyn_nodes
//           .append("g")
//           .attr("id","id"+frag.id)
//           .selectAll(".active_node")
//           .data(children)
//           .enter()
//           .append("circle")
//           .attr("class","child_node")
//           .attr("cx",d=>chr_radius*Math.cos(d.radian))
//           .attr("cy",d=>chr_radius*Math.sin(d.radian))
//           .attr("r",d=> parent_size_scale(d.end-d.start))
//           .attr("fill",color("child_node"));
//     // Infobar
//     const infobar = info
//           .append("div")
//           .attr("id","id" + frag.id);
//     infobar
//         .selectAll(".source")
//         .data([frag])
//         .enter()
//         .append("div")
//         .attr("class","source")
//         .text(parent_info);
//     infobar.append("div")
//         .append("ul")
//         .selectAll(".target")
//         .data(children)
//         .enter()
//         .append("li")
//         .attr("class","target")
//         .html(child_info);
// };

// const mouseOut = frag => {
//     console.log("OUT" + frag.id);
//     const target = d3.select(d3.event.target);
//     if (!target.classed("is_clicked")){
//         dyn_links.select("#id" + frag.id).remove();
//         dyn_nodes.select("#id" + frag.id).remove();
//         info.select("#id" + frag.id).remove();
//         target.classed("active_node",false);
//     }
// };

// const mouseClick = frag => {
//     const target = d3.select(d3.event.target);
//     if (target.classed("is_clicked")){
//         target.classed("is_clicked",false);
//         dyn_links.select("#id" + frag.id).remove();
//         dyn_nodes.select("#id" + frag.id).remove();
//         info.select("#id" + frag.id).remove();
//     }else{
//         target.classed("is_clicked",true);
//     }
// };
