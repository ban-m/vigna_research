#!/bin/bash
#$ -S /bin/bash
#$ -N G_vs_G
#$ -cwd
#$ -pe smp 24
#$ -e ./logfiles/genome_vs_genome.log
#$ -o ./logfiles/genome_vs_genome.out
#$ -V
#$ -m e
# -M banmasutani@gmail.com ## To send a mail when ended
# -t 1:n ## For array job
OUTPUT=/grid/ban-m/vigna_species/genome_compare/
ROOT=/grid/ban-m/vigna/Vigna_genomes_and_annotations/
mkdir -p ${OUTPUT}
TEMP=./script/genome_vs_genome_minimap2.sh

echo "#!/bin/bash" > ${TEMP}

cargo run --release --bin genome_vs_genome -- \
      ${ROOT} \
      angularis  exilis  indica  marina  minima  mungo  riukiuensis  stipulacea  trilobata  vexillata \
      ${OUTPUT} >> ${TEMP}

bash ${TEMP}

